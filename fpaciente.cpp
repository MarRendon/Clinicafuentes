#include "fpaciente.h"
#include "ui_fpaciente.h"
#include "paciente.h"
#include "QDebug"
#include "v_buscapaciente.h"
#include "muestrapaciente.h"
#include <string>
#include <qdebug.h>
#include <fstream>
#include <QMessageBox>
#include <QRegExpValidator>


fpaciente::fpaciente(QString* FN, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::fpaciente)
{
    ui->setupUi(this);
    this->fechaN = *FN;
    m_pPalette	= new QPalette();
    m_pPixmap		= new QPixmap("/home/maralba/Imágenes/bg3.jpg");

    m_pPalette->setBrush(QPalette::Background,QBrush(*m_pPixmap));
    setPalette(*m_pPalette);

    QRegExp rx1("^[0-9]*$");
    QRegExpValidator *valiT= new QRegExpValidator(rx1, this);
    ui->numerohistoria->setValidator(valiT);
    /**
      * @brief Muestra en QTableWidget los paciente con la fecha de nacimiento buscada
      */

     Paciente p;

     std::fstream archivo;
     archivo.open("./pacientes.txt", std::ios::in | std::ios::out | std::ios::app);

     if(archivo.bad())
     {
         qDebug() << "ERROR al abrir el archivo pacientes.txt -> ventana fpacientes";
     }

     int tam=0, encontro=0;
     std::string aux, aux2;

     aux = this->fechaN.toStdString();
     int pos;
     archivo.seekg(0, archivo.beg);

     while(!archivo.eof())
     {
         archivo >> p;
         aux2= p.ver_fecha_nacimiento();
         pos=0;

         if(archivo.good())
         {
             pos = aux2.find(aux);

             if(pos!=-1)
             {
                  ui->tabla->insertRow(ui->tabla->rowCount());
                  ui->tabla->setItem(ui->tabla->rowCount()-1,0,new QTableWidgetItem(QString::fromStdString(aux)));
                  ui->tabla->setItem(ui->tabla->rowCount()-1,1,new QTableWidgetItem(QString::fromStdString(p.ver_nombre())));
                  ui->tabla->setItem(ui->tabla->rowCount()-1,2,new QTableWidgetItem(QString::number(p.ver_numero_historia())));
             }else{
                  encontro++;
             }
             tam++;
         }
     }

     if(encontro==tam)
     {
         QMessageBox::information(this, tr("ERROR"), tr("Fecha no encontrada"));
         v_buscapaciente *fpaciente= new v_buscapaciente;
         fpaciente->show();
         this->close();
     }
}


fpaciente::~fpaciente()
{
    delete ui;
}

void fpaciente::on_volver_clicked()
{
    v_buscapaciente *fpaciente = new v_buscapaciente;
    fpaciente->show();
    this->close();
}

void fpaciente::on_buscar_clicked()
{
    int n_historia;
    Paciente p;
    std::string aux("NULL");
    n_historia= this->ui->numerohistoria->text().toInt();

    std::ifstream archivo;
    archivo.open("./pacientes.txt");

    archivo.seekg(0, archivo.beg);
    archivo.seekg(((n_historia-1)*79), archivo.beg);

    archivo >> p;

    if(aux!=p.ver_nombre())
    {
         MuestraPaciente *v_buscapaciente = new MuestraPaciente(&n_historia, &p);
         v_buscapaciente->show();
         this->close();

    }else{

        QMessageBox::information(this, tr("ERROR"), tr("Historia no encontrada"));
        v_buscapaciente *fpaciente= new v_buscapaciente;
        fpaciente->show();
        this->close();
    }
}
