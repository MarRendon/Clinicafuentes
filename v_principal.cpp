#include "v_principal.h"
#include "ui_v_principal.h"
#include "v_buscapaciente.h"
#include "v_paciente.h"
#include "v_programa.h"
#include <QtWidgets>
/**
 * @brief Asigna un fondo a la ventana v_principal
 * @param parent
 */
v_principal::v_principal(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::v_principal)
{
    ui->setupUi(this);

    m_pPalette	= new QPalette();
    m_pPixmap		= new QPixmap("/home/maralba/Imágenes/bg3.jpg");

    m_pPalette->setBrush(QPalette::Background,QBrush(*m_pPixmap));
    setPalette(*m_pPalette);

    setWindowTitle("Bienvenido");
}

v_principal::~v_principal()
{
    delete ui;
}

/**
 * @brief boton para abrir la ventana v_paciente (agrega un nuevo paciente al archivo)
 */
void v_principal::on_b_nuevopaciente_clicked()
{
   v_paciente *v_principal = new v_paciente;
   v_principal->show();
   this->close();
}

/**
 * @brief abre la ventana v_buscapaciente
 */
void v_principal::on_b_paciente_clicked()
{
    v_buscapaciente *v_principal = new v_buscapaciente;
    v_principal->show();
    this->close();
}

/**
 * @brief Sale del programa
 */
void v_principal::on_pushButton_clicked()
{
    this->close();
}
