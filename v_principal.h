#ifndef V_PRINCIPAL_H
#define V_PRINCIPAL_H

#include <QMainWindow>
//#include<paciente.h>

namespace Ui {
class v_principal;
}
/**
 * @brief The v_principal class, crea la ventana principal
 */
class v_principal : public QMainWindow
{
    Q_OBJECT

public:
    explicit v_principal(QWidget *parent = 0);
    ~v_principal();

    void resizeEvent (QResizeEvent* event)
    {
        m_pPalette->setBrush(QPalette::Background,QBrush(m_pPixmap->scaled(width(),height())));
        setPalette(*m_pPalette);
    };

private slots:
    /**
     * @brief on_b_nuevopaciente_clicked, Abre la ventana v_paciente para introducir datos del nuevo paciente
     */
    void on_b_nuevopaciente_clicked();
    /**
     * @brief on_b_paciente_clicked,  Abre la ventana v_buscapaciente para introducir numero de historia del paciente
     */
    void on_b_paciente_clicked();
    /**
     * @brief on_pushButton_clicked, Sale de la ventana v_principal
     */
    void on_pushButton_clicked();

private:
    QPixmap* m_pPixmap;
    QPalette* m_pPalette;
    Ui::v_principal *ui;
};

#endif // V_PRINCIPAL_H
