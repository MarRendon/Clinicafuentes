#include "datoscita.h"
#include "ui_datoscita.h"
#include"cita.h"
#include"fstream"
#include"QDebug"
#include"v_programa.h"
#include<string>
#include<QRegExpValidator>
#include<QMessageBox>

/**
 * @brief Valida y almacena los datos para una cita
 */
datosCita::datosCita(int *nh, std::string *f, int ho[], int tam, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::datosCita)
{
    ui->setupUi(this);
    this->n_historia=*nh;
    this->fecha=*f;

    for(int i=0; i<8; i++)
    {
        this->hocupadas[i]= ho[i];
    }

    QRegExp rx("^([a-z]+[,.]?[ ]?|[a-z]+['-]?)+$");
    QRegExpValidator *valiM= new QRegExpValidator(rx, this);
    ui->lineEdit_motivo->setValidator(valiM);

    QRegExp rx1("^[0-9]*$");
    QRegExpValidator *valiT= new QRegExpValidator(rx1, this);
    ui->lineEdit_hora->setValidator(valiT);
    ui->lineEdit_coddoc->setValidator(valiT);

    m_pPalette	= new QPalette();
    m_pPixmap		= new QPixmap("/home/maralba/Imágenes/imagen11.jpg");

    m_pPalette->setBrush(QPalette::Background,QBrush(*m_pPixmap));
    setPalette(*m_pPalette);

    this->setWindowTitle("Datos de la cita");
}

datosCita::~datosCita()
{
    delete ui;
}

/**
 * @brief Guarda los datos de la cita en el archivo "citas.txt"
 */
void datosCita::on_pushButton_Guardar_clicked()
{
    Cita c;
    int tam, hora, error=0;
    std::fstream lecturaEscritura;
    lecturaEscritura.open("./citas.txt", std::ios::in | std::ios::out | std::ios::app);

    if(lecturaEscritura.bad()){
          qDebug()<<"error al abrir el archivo, datoscita";
    }

    hora=ui->lineEdit_hora->text().toInt();

    for(int i=0; i<8; i++)
    {
        if((this->hocupadas[i]==hora) || (hora==1) || (hora==12) || ((hora>5 && (hora<8))))
        {
            error++;
        }
    }

    if(!error)
    {
        c.ingresar_fecha(this->fecha);
        c.ingresar_hora(hora);

        if( ui->lineEdit_motivo->text().isEmpty())
        {
          QMessageBox::information(this, tr("ERROR"), tr("Debe indicar el motivo de la cita"));
          error++;
        }
        else
        {
          c.ingresar_motivo(ui->lineEdit_motivo->text().toStdString());
        }

        if( ui->lineEdit_coddoc->text().isEmpty())
        {
          QMessageBox::information(this, tr("ERROR"), tr("Debe indicar el código del doctor que atendera la cita"));
          error++;
        }
        else
        {
            c.ingresar_doctor(ui->lineEdit_coddoc->text().toInt());
        }

        c.ingresar_n_historiap(this->n_historia);

        if(!error)
        {
            lecturaEscritura.seekg(0, lecturaEscritura.end);
            tam = lecturaEscritura.tellg();
            tam=tam/55;
            c.ingresar_posicion(tam+1);

            lecturaEscritura << c;
            QMessageBox::information(this, tr("Éxito"), tr("Cita guardada con Éxito"));
        }

        v_programa *datosCita= new v_programa(&this->n_historia);
        datosCita->show();
        this->close();
    }
    else
    {
         QMessageBox::information(this, tr("ERROR"), tr("Hora no disponible"));
    }
}

/**
 * @brief regresa a la ventana con el prorama
 */

void datosCita::on_pushButton_salir_clicked()
{
    v_programa *datosCita= new v_programa(&this->n_historia);
    datosCita->show();
    this->close();
}
