#include "v_programa.h"
#include "ui_v_programa.h"
#include "cita.h"
#include "paciente.h"
#include "datoscita.h"
#include "v_principal.h"
#include "muestracita.h"
#include <fstream>
#include <QDebug>
#include <QMessageBox>
/**
 * @brief recibe el número de historia y establece un fondo al formulario
 * @param nh numero de historia
 * @param parent
 */
v_programa::v_programa(int* nh, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::v_programa)
{
    ui->setupUi(this);
    this->n_historia=*nh;
    ui->fecha_3->setDisplayFormat("dd/MM/yyyy");
    /**
      inicializa el vector en 0
     */

    for(int i=0; i<8; i++)
    {
       this->ho[i]=0;
    }

    /**
      Fondo de la ventana
      **/
    m_pPalette	= new QPalette();
    m_pPixmap		= new QPixmap("/home/maralba/Imágenes/bg3.jpg");

    m_pPalette->setBrush(QPalette::Background,QBrush(*m_pPixmap));
    setPalette(*m_pPalette);

    ui->fecha_3->setDate(ui->calendar_3->selectedDate());
    this->setFixedSize(this->size());
    connect(ui->fecha_3,SIGNAL(dateChanged(QDate)),ui->calendar_3,SLOT(setSelectedDate(QDate)));

    setWindowTitle("Programa");
}

v_programa::~v_programa()
{
    delete ui;
}

/**
 * @brief muestra la disponibilidad de cada fecha y hora dependiendo del archivo "citas.txt"
 */
void v_programa::on_calendar_3_selectionChanged()
{
    ui->fecha_3->setDate(ui->calendar_3->selectedDate());

    std::string actual;
    int cont=0, i, t[8], pos[8];
    Cita c;
    QString str = ui->fecha_3->date().toString("dd/MM/yyyy");

    actual=str.toStdString();

    for(i=0; i<8; i++)
    {
       ho[i]=0;
       t[i]=0;
       pos[i]=0;
    }

    std::fstream lecturaEscritura;
    lecturaEscritura.open("./citas.txt", std::ios::in | std::ios::out | std::ios::app);

    if(lecturaEscritura.bad()){
          qDebug()<<"error al abrir el archivo v_programa selectionchange";
    }

    bool encontro=false;

    while(!lecturaEscritura.eof())
    {
      lecturaEscritura >> c;

      if(lecturaEscritura.bad())
      {
              qDebug()<<"error al abrir el archivo elseif";
      }else{

         if(c.ver_fecha()==actual)
         {
             ho[cont]=c.ver_hora();
             pos[cont]=c.ver_posicion();
             encontro=true;
             cont++;
         }
      }
    }

    if(encontro)
    {
        for(i=0; i<cont; i++)
        {
            //1
            if(ho[i]==8 && !t[0])
            {
                 ui->Tabla->setItem(0,0, new QTableWidgetItem("Ocupado"));
                 ui->Tabla->setItem(0,1, new QTableWidgetItem(QString::number(pos[i])));
                 t[0]++;
            }else if(!t[0]){
                  ui->Tabla->setItem(1,0, new QTableWidgetItem("Disponible"));
            }
            //2
            if(ho[i]==9 && !t[1])
            {
                 ui->Tabla->setItem(1,0, new QTableWidgetItem("Ocupado"));
                 ui->Tabla->setItem(1,1, new QTableWidgetItem(QString::number(pos[i])));
                 t[1]++;
            }else if(!t[1]){
                 ui->Tabla->setItem(1,0, new QTableWidgetItem("Disponible"));
            }
            //3
            if(ho[i]==10 && !t[2])
            {
                 ui->Tabla->setItem(2,0, new QTableWidgetItem("Ocupado"));
                 ui->Tabla->setItem(2,1, new QTableWidgetItem(QString::number(pos[i])));
                 t[2]++;
            }else if(!t[2]){
                 ui->Tabla->setItem(2,0, new QTableWidgetItem("Disponible"));
            }
            //4
            if(ho[i]==11 && !t[3])
            {
                 ui->Tabla->setItem(3,0, new QTableWidgetItem("Ocupado"));
                 ui->Tabla->setItem(3,1, new QTableWidgetItem(QString::number(pos[i])));
                 t[3]++;
            }else if(!t[3]){
                 ui->Tabla->setItem(3,0, new QTableWidgetItem("Disponible"));
            }
            //5
            if(ho[i]==2 && !t[4])
            {
                 ui->Tabla->setItem(4,0, new QTableWidgetItem("Ocupado"));
                 ui->Tabla->setItem(4,1, new QTableWidgetItem(QString::number(pos[i])));
                 t[4]++;
            }else if(!t[4]){
                 ui->Tabla->setItem(4,0, new QTableWidgetItem("Disponible"));
            }
            //6
            if(ho[i]==3 && !t[5])
            {
                 ui->Tabla->setItem(5,0, new QTableWidgetItem("Ocupado"));
                 ui->Tabla->setItem(5,1, new QTableWidgetItem(QString::number(pos[i])));
                 t[5]++;
            }else if(!t[5]){
                 ui->Tabla->setItem(5,0, new QTableWidgetItem("Disponible"));
            }
            //7
            if(ho[i]==4 && !t[6])
            {
                 ui->Tabla->setItem(6,0, new QTableWidgetItem("Ocupado"));
                 ui->Tabla->setItem(6,1, new QTableWidgetItem(QString::number(pos[i])));
                 t[6]++;
            }else if(!t[6]){
                 ui->Tabla->setItem(6,0, new QTableWidgetItem("Disponible"));
            }
            //8
            if(ho[i]==5 && !t[7])
            {
                 ui->Tabla->setItem(7,0, new QTableWidgetItem("Ocupado"));
                 ui->Tabla->setItem(7,1, new QTableWidgetItem(QString::number(pos[i])));
                 t[7]++;
            }else if(!t[7]){
                 ui->Tabla->setItem(7,0, new QTableWidgetItem("Disponible"));
            }
        }

    }else{           
            for(i=0; i<8;i++)
            {
               ui->Tabla->setItem(i,0, new QTableWidgetItem("Disponible"));
            }
        }
}

/**
 * @brief Boton: hacer cita, Guarda la fecha en el archivo "citas.txt", abre y pasa el número de historia a la ventana datoscita
 */
void v_programa::on_guardar_3_clicked()
{
    ui->fecha_3->setDate(ui->calendar_3->selectedDate());

    Cita c;
    QString str = ui->fecha_3->date().toString("dd/MM/yyyy");
    std::string fecha;
    fecha = str.toStdString();

    int tam=9;
    datosCita *v_programa= new datosCita(&this->n_historia, &fecha, this->ho, tam);
    v_programa->show();
    this->close();
}

/**
 * @brief Regresa a la ventana v_principal
 */
void v_programa::on_volver_clicked()
{
    v_principal *v_programa = new v_principal;
    v_programa->show();
    this->close();
}

void v_programa::on_eliminar_clicked()
{
    Cita c;
    int pos, hora;

    pos = ui->line_posicion->text().toInt(); 

    std::ifstream original;
    original.open("./citas.txt");

    if(original.bad()){
          qDebug()<<"error al abrir el archivo, datoscita";
    }

    std::ofstream actual;
    actual.open("./aux.txt");

    if(actual.bad()){
          qDebug()<<"error al abrir el archivo, datoscita";
    }

    bool encontro=false;

    while(!original.eof())
    {
        original >> c;

        if(original.good()){

            if(c.ver_posicion() == pos){
                encontro=true;
            }
        }
    }

    original.clear();
    original.seekg(0, original.beg);

    if(!encontro)
    {
        QMessageBox::information(this, tr("ERROR"), tr("Cita no encontrada"));

    }else{

        while(!original.eof())
        {
            original >> c;

            if(original.good()){

                if(c.ver_posicion() != pos){
                    actual << c;
                }
                else{
                    hora= c.ver_hora();
                }
            }
        }

        rename("citas.txt", "citas.old");
        rename("aux.txt", "citas.txt");
        remove("citas.old");

        QMessageBox::information(this, tr("Éxito"), tr("Cita Eliminado Satisfactoriamente"));
    }

    original.close();
    actual.close();

    for(int i=0; i<8; i++)
    {
        if(this->ho[i]==hora)
        {
           this->ho[i]=0;
        }
    }
}

void v_programa::on_buscar_clicked()
{
    int pos = ui->lvercita->text().toInt();
    muestracita *v_programa = new muestracita(&pos, &this->n_historia);
    v_programa->show();
}
