#ifndef DATOSCITA_H
#define DATOSCITA_H
#include<string>
#include <QWidget>

namespace Ui {
class datosCita;
}
/**
 * @brief Ventana para obtener los datos de la cita
 */
class datosCita : public QWidget
{
    Q_OBJECT

public:

    /**
     * @brief n_historia numero de historia del paciente
     */
    int n_historia;
    /**
     * @brief hocupadas guarda las horas ocupadas para un dia, valida que no se hagan dos citas
     */
    int hocupadas[8];
    /**
     * @brief fecha fecha de la cita
     */
    std::string fecha;


    explicit datosCita(int *nh, std::string *f, int [], int tam,  QWidget *parent = 0);
    ~datosCita();

private slots:
    /**
     * @brief Redimensiona la imagen de fondo
     * @param event
     */
    void resizeEvent (QResizeEvent* event)
    {
        m_pPalette->setBrush(QPalette::Background,QBrush(m_pPixmap->scaled(width(),height())));
        setPalette(*m_pPalette);
    };

    /**
     * @brief Guarda los datos en el archivo "citas.txt"
     */
    void on_pushButton_Guardar_clicked();
    /**
     * @brief Regresa a la v_programa
     */
    void on_pushButton_salir_clicked();

private:

    QPixmap* m_pPixmap;
    QPalette* m_pPalette;

    Ui::datosCita *ui;
};

#endif // DATOSCITA_H
