#include"paciente.h"
#include<iomanip>
#include<stdlib.h>
#include<string>
#include<iostream>

using std::setw;
using std::setprecision;
using std::left;
using std::right;

using namespace  std;

/**
 * @brief Constructor
 */
Paciente::Paciente()
{
    this->nombre="NULL";
    this->seguro="NULL";
    this->telefono="NULL";
    this->numero_historia=0;
    this->fecha_nacimiento="NULL";

    return;
}

Paciente::Paciente(const Paciente &p){

    this->nombre=p.nombre;
    this->seguro=p.seguro;
    this->telefono=p.telefono;
    this->numero_historia=p.numero_historia;
    this->fecha_nacimiento=p.fecha_nacimiento;

    return;
}
/**
 * @brief guarda nombre
 * @param name
 * @return nombre
 */
void Paciente::ingresar_nombre(string name){

    this->nombre=name;
}

string Paciente::ver_nombre() const{  return(this->nombre);}

/**
 * @brief guarda fecha de nacimiento
 */
void Paciente::ingresar_fecha_nacimiento(string d){

    this->fecha_nacimiento = d;
}

string Paciente::ver_fecha_nacimiento() const{  return(this->fecha_nacimiento);}

/**
 * @brief guarda seguro
 * @param s
 * @return seguro
 */
void Paciente::ingresar_seguro(string s){

   this->seguro=s;
}

string Paciente::ver_seguro() const{  return(this->seguro);}

/**
 * @brief Guarda telefono
 * @param t
 * @return telefono
 */
void Paciente::ingresar_telefono(string t){

   this->telefono=t;
}

string Paciente::ver_telefono() const{    return(this->telefono);}

/**
 * @brief guarda número de historia
 * @param nh
 * @return numero de historia
 */
void Paciente::ingresar_numero_historia(int nh){

    this->numero_historia=nh;
}

/**
 * @brief sobrecarga del operador <<
 * @return
 */
int Paciente::ver_numero_historia()  const{    return(this->numero_historia);}

std::ostream& operator <<(std::ostream& s, const Paciente& p){

   s << left << setw(10) << p.ver_numero_historia() << ":";
   s << left << setw(15) << p.ver_nombre() << ":";
   s << left << setw(15) << p.ver_fecha_nacimiento() << ":";
   s << left << setw(20) << p.ver_seguro() << ":";
   s << right << setw(15) << p.ver_telefono() << '\n';

   return(s);
}

/**
 * @brief sobrecarga del operator >>
 * @param s
 * @param p
 * @return
 */
std::istream& operator >>(std::istream& s, Paciente& p){

    std::string cadenaAuxiliar;
    int f[3];

    std::getline(s, cadenaAuxiliar, ':');

    if (!s.fail()) {
       f[0]=(int)atoi(cadenaAuxiliar.c_str());
       p.ingresar_numero_historia(f[0]);
    }

    std::getline(s, cadenaAuxiliar, ':');

    if (!s.fail()) {
       p.ingresar_nombre(cadenaAuxiliar);
    }

    std::getline(s, cadenaAuxiliar, ':');

    if (!s.fail())
       p.ingresar_fecha_nacimiento(cadenaAuxiliar);

    std::getline(s, cadenaAuxiliar, ':');

    if (!s.fail()) {
       p.ingresar_seguro(cadenaAuxiliar);
    }

    std::getline(s, cadenaAuxiliar);

    if (!s.fail()) {
       p.ingresar_telefono(cadenaAuxiliar);
    }

    return(s);

}
