#include "muestracita.h"
#include "ui_muestracita.h"
#include "cita.h"
#include "paciente.h"
#include "v_programa.h"
#include <QMessageBox>
#include <fstream>
#include "QDebug"

muestracita::muestracita(int* posicion, int *nh, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::muestracita)
{
    ui->setupUi(this);

    m_pPalette	= new QPalette();
    m_pPixmap		= new QPixmap("/home/maralba/Imágenes/imagen15.jpg");

    m_pPalette->setBrush(QPalette::Background,QBrush(*m_pPixmap));
    setPalette(*m_pPalette);
    this->n_historia=*nh;
    Cita c2;
    Paciente p;
    int pos= *posicion;

    std::ifstream original;
    original.open("./citas.txt");

    std::ifstream paciente;
    paciente.open("./pacientes.txt");

    if(original.bad()){
          qDebug() <<"error al abrir el archivo, muestracita original";
    }

    if(paciente.bad()){
          qDebug() <<"error al abrir el archivo, muestracita paciente";
    }

    bool encontro=false;

    while(!original.eof())
    {
        original >> c2;

        if(original.good()){

            if(c2.ver_posicion() == pos){

                ui->Fecha->setText(QString::fromStdString(c2.ver_fecha()));
                ui->nh->setText(QString::number(c2.ver_n_historiap()));
                ui->hora->setText(QString::number(c2.ver_hora()));
                ui->motivo->setText(QString::fromStdString(c2.ver_motivo()));
                ui->Cdoctor->setText(QString::number(c2.ver_doctor()));

                encontro=true;
                break;

            }
        }
    }

    if(!encontro)
    {
        ui->Fecha->setText(" ");
        ui->hora->setText(0);
        ui->motivo->setText(" ");
        ui->Cdoctor->setText(" ");

        QMessageBox noc;
        QString er = QString::number(pos);
        QString s = tr("No se encontro la cita número: %1").arg(er);
        noc.setText(s);
        noc.exec();
    }

    original.close();
}

muestracita::~muestracita()
{
    delete ui;
}

void muestracita::on_pushButton_clicked()
{
    v_programa *muestracita = new v_programa(&this->n_historia);
    muestracita->show();
    this->close();
}
