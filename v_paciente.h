#ifndef V_PACIENTE_H
#define V_PACIENTE_H

#include "paciente.h"
#include <QWidget>

namespace Ui {
class v_paciente;
}
/**
 * @brief The v_paciente class, Crea la ventana en la que se introduciran los datos del nuevo paciente
 */
class v_paciente : public QWidget
{
    Q_OBJECT

public:

    void resizeEvent (QResizeEvent* event)
    {
        m_pPalette->setBrush(QPalette::Background,QBrush(m_pPixmap->scaled(width(),height())));
        setPalette(*m_pPalette);
    };

    explicit v_paciente(QWidget *parent = 0);
    ~v_paciente();

private slots:
    /**
     * @brief on_pushButton_clicked, Boton que regresa a la ventana principal
     */
    void on_pushButton_clicked();
    /**
     * @brief on_pushButton_npaciente_guardar_clicked, Guarda los datos del nuevo Paciente
     */
    void on_pushButton_npaciente_guardar_clicked();

  //  void on_pushButton_hacercita_clicked();

private:

    QPixmap* m_pPixmap;
    QPalette* m_pPalette;

    Ui::v_paciente *ui;
    /**
     * @brief p objeto de la clase Paciente para guardar cada uno de los datos en el archivo utilizando la sobrecarga de la misma
     */
    Paciente p;

};

#endif // V_PACIENTE_H
