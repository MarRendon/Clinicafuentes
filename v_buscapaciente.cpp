#include "v_buscapaciente.h"
#include "ui_v_buscapaciente.h"
#include"v_principal.h"
#include<fstream>
#include"paciente.h"
#include"muestrapaciente.h"
#include"v_paciente.h"
#include "fpaciente.h"
#include <string>
#include<QMessageBox>
#include<QRegExpValidator>


/**
 * @brief constructor, asigna una imagen a la ventana v_buscapaciente
 * @param parent
 */
v_buscapaciente::v_buscapaciente(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::v_buscapaciente)
{
    ui->setupUi(this);
    ui->fechan->setDisplayFormat("dd/MM/yyyy");

    QRegExp rx("^[0-9]*$");
    QRegExpValidator *valiN= new QRegExpValidator(rx, this);
    ui->lineEdit_nhistoria->setValidator(valiN);

    QLabel* pLabel = new QLabel;
    pLabel->setStyleSheet("QLabel { background-color : red; color : blue; }");

    m_pPalette	= new QPalette();
    m_pPixmap		= new QPixmap("/home/maralba/Imágenes/bg3.jpg");

    m_pPalette->setBrush(QPalette::Background,QBrush(*m_pPixmap));
    setPalette(*m_pPalette);

    setWindowTitle("Buscar");
}

v_buscapaciente::~v_buscapaciente()
{
    delete ui;
}

/**
 * @brief busca a el paciente por el numero de historia (el número de historia equivale a su posicion en el archivo + 1)
 */
void v_buscapaciente::on_b_buscar_clicked()
{
    //Busca un paciente por el numero de historia
    //el numero de historia menos uno, corresponde a la posicion en el registro

    int n_historia, pos;
    Paciente p;
    std::string aux("NULL"), aux2;
    n_historia= this->ui->lineEdit_nhistoria->text().toInt();

    std::ifstream archivo;
    archivo.open("./pacientes.txt");

    archivo.seekg(0, archivo.beg);
    archivo.seekg(((n_historia-1)*79), archivo.beg);

    archivo >> p;

    aux2=p.ver_nombre();

    pos = aux2.find(aux);

    if(pos==-1)
    {
         MuestraPaciente *v_buscapaciente = new MuestraPaciente(&n_historia, &p);
         v_buscapaciente->show();
         this->close();

    }else{

        QMessageBox::information(this, tr ("ERROR"), tr("Historia no encontrada"));
    }
}

/**
 * @brief regresa a la ventana v_principal
 */
void v_buscapaciente::on_pushButton_bpac_volver_clicked()
{
    v_principal *v_buscapaciente = new v_principal;
    v_buscapaciente->show();
    this->close();
}

void v_buscapaciente::on_pushButton_clicked()
{
    //Busca un paciente por fecha de nacimiento

    QString fecha_n =  ui->fechan->date().toString("dd/MM/yyyy");

    fpaciente *v_buscapaciente = new fpaciente(&fecha_n);
    v_buscapaciente->show();
    this->close();
}
