#ifndef MOFICASEGURO_H
#define MOFICASEGURO_H

#include <QWidget>

namespace Ui {
class moficaseguro;
}
/**
 * @brief Crea la ventana para moficar el seguro del paciente
 */
class moficaseguro : public QWidget
{
    Q_OBJECT

public:


    void resizeEvent (QResizeEvent* event)
    {
        m_pPalette->setBrush(QPalette::Background,QBrush(m_pPixmap->scaled(width(),height())));
        setPalette(*m_pPalette);
    };


    /**
     * @brief Numero de historia del paciente a modificar
     */
    int n_historia;
    explicit moficaseguro(int *, QWidget *parent = 0);
    ~moficaseguro();

private slots:
    /**
     * @brief Guarda los datos modificados del paciente
     */
    void on_pushButton_guardar_clicked();

private:

    QPixmap* m_pPixmap;
    QPalette* m_pPalette;

    Ui::moficaseguro *ui;

};

#endif // MOFICASEGURO_H
