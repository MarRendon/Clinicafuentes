#ifndef V_BUSCAPACIENTE_H
#define V_BUSCAPACIENTE_H

#include <QWidget>

namespace Ui {
class v_buscapaciente;
}
/**
 * @brief Crea la ventana v_buscapaciente, la cual recibe un número de historia y realiza la busqueda
 */
class v_buscapaciente : public QWidget
{
    Q_OBJECT

public:

    /**
     * @brief resizeEvent redimensiona la imagen de fondo
     * @param event
     */
    void resizeEvent (QResizeEvent* event)
    {
        m_pPalette->setBrush(QPalette::Background,QBrush(m_pPixmap->scaled(width(),height())));
        setPalette(*m_pPalette);
    };

    explicit v_buscapaciente(QWidget *parent = 0);
    ~v_buscapaciente();

private slots:
    /**
     * @brief activa la busqueda
     */
    void on_b_buscar_clicked();
    /**
     * @brief Regresa a la ventana principal
     */
    void on_pushButton_bpac_volver_clicked();

    void on_pushButton_clicked();

private:

    QPixmap* m_pPixmap;
    QPalette* m_pPalette;

    Ui::v_buscapaciente *ui;
};

#endif // V_BUSCAPACIENTE_H
