#ifndef CITA
#define CITA
#include<string>

using std::string;
/**
 * @brief Guarda los datos necesarios para una cita
 */
class Cita{

    private:

        string fecha;
        string motivo;
        int hora;
        int cod_doctor;
        int n_historiap;
        int posicion;
        /**
         * @brief posicion, en caso de querer eliminar una cita   
         */

    public:
        /**
         * @brief constructor por defecto
         */
        Cita();

        /**
         * @brief constructor de copia
         */
        Cita(const Cita &);

        ~Cita(){}

        /**
         * @brief Guarda la fecha de la cita
         */
        void ingresar_fecha(string);
        /**
         * @brief ver_fecha
         * @return fecha de la cita
         */
        string ver_fecha() const;

        /**
         * @brief guarda la hora en la que se efectuara la cita
         */
        void ingresar_hora(int);
        /**
         * @brief ver_hora
         * @return hora de la cita
         */
        int ver_hora()  const;

        /**
         * @brief guarda el motivo de la cita
         */
        void ingresar_motivo(string);
        /**
         * @brief ver_motivo
         * @return motivo de la cita
         */
        string ver_motivo()  const;

        /**
         * @brief guarda el código del doctor que atendera al paciente
         */
        void ingresar_doctor(int);
        /**
         * @brief ver_doctor
         * @return codigo del doctor que atendera la cita
         */
        int ver_doctor()  const;

        /**
         * @brief guarda el número de historia del paciente que asistira a la cita
         */
        void ingresar_n_historiap(int);
        /**
         * @brief ver_n_historiap
         * @return Numero de historia del paciente que asistira a la cita
         */
        int ver_n_historiap()  const;

        /**
          * @brief ingresar_posicion guarda el código de la cita
          */
         void ingresar_posicion(int);

         /**
          * @brief ver_posicion
          * @return retorna el código de la cita
          */
         int ver_posicion()  const;
};

/**
 * @brief sobrecarga del operados operator << para guardar las citas en el archivo
 * @param o archivo
 * @param c objet de la clase
 * @return o
 */
std::ostream& operator <<(std::ostream& o, const Cita& c);

/**
 * @brief sobrecarga del operados operator >> para extraer registros del archivo "citas.txt"
 * @param o archivo
 * @param c objet de la clase
 * @return o
 */
std::istream& operator >>(std::istream& s, Cita& c);

#endif
