#ifndef MODIFICATLF_H
#define MODIFICATLF_H

#include <QWidget>

namespace Ui {
class modificatlf;
}

/**
 * @brief Modifica el número de telefono del paciente actual
 */
class modificatlf : public QWidget
{
    Q_OBJECT

public:


    void resizeEvent (QResizeEvent* event)
    {
        m_pPalette->setBrush(QPalette::Background,QBrush(m_pPixmap->scaled(width(),height())));
        setPalette(*m_pPalette);
    };


    /**
     * @brief guarda el numero de historia del paciente que se esta modificando
     */
    int n_historia;
    explicit modificatlf(int *nhistoria, QWidget *parent = 0);
    ~modificatlf();

private slots:
    /**
     * @brief Guarda los datos modificados del paciente
     */
    void on_pushButton_guardar_clicked();

private:

    QPixmap* m_pPixmap;
    QPalette* m_pPalette;

    Ui::modificatlf *ui;
};

#endif // MODIFICATLF_H
