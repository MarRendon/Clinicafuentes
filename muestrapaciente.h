#ifndef MUESTRAPACIENTE_H
#define MUESTRAPACIENTE_H
#include"paciente.h"
#include <QWidget>

namespace Ui {
class MuestraPaciente;
}
/**
 * @brief Crea la ventana que mostrara los datos del paciente buscado
 */
class MuestraPaciente : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief redimensiona la imagen de fondo
     */

    void resizeEvent (QResizeEvent* event)
    {
        m_pPalette->setBrush(QPalette::Background,QBrush(m_pPixmap->scaled(width(),height())));
        setPalette(*m_pPalette);
    };

    explicit MuestraPaciente(int *nhistoria, Paciente *a, QWidget *parent = 0);
    ~MuestraPaciente();
    /**
     * @brief p paciente buscado
     */
    Paciente p;
    /**
     * @brief n_historia numero de historia del paciente a buscar
     */
    int n_historia;

private slots:

    /**
     * @brief Vuelve a la ventana v_buscapaciente
     */
    void on_pushButton_volver_clicked();
    /**
     * @brief Modifica el seguro del paciente
     */
    void on_pushButton_modificar_clicked();
    /**
     * @brief Modifica el número de telefono del paciente
     */
    void on_pushButton_modificar_2_clicked();
    /**
     * @brief Abre la ventana v_programa para ver la disponibilidad
     */
    void on_pushButton_hacercita_clicked();
    /**
     * @brief Elimina el paciente mostrado
     */
    void on_pushButton_eliminar_clicked();

    void on_pushButton_clicked();

private:

    QPixmap* m_pPixmap;
    QPalette* m_pPalette;

    Ui::MuestraPaciente *ui;

};

#endif // MUESTRAPACIENTE_H
