#include "moficaseguro.h"
#include "ui_moficaseguro.h"
#include"v_buscapaciente.h"
#include "paciente.h"
#include <fstream>
#include <QMessageBox>
#include <QRegExpValidator>
/**
 * @brief modifica seguro del paciente
 * @param nhistoria
 * @param parent
 */
moficaseguro::moficaseguro(int *nhistoria, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::moficaseguro)
{
        ui->setupUi(this);

        m_pPalette	= new QPalette();
        m_pPixmap		= new QPixmap("/home/maralba/Imágenes/imagen15.jpg");

        m_pPalette->setBrush(QPalette::Background,QBrush(*m_pPixmap));
        setPalette(*m_pPalette);

        QRegExp rx("^([a-z]+[,.]?[ ]?|[a-z]+['-]?)+$");
        QRegExpValidator *valiS= new QRegExpValidator(rx, this);
        ui->lineEdit_seguro->setValidator(valiS);

        this->n_historia=*nhistoria;
        setWindowTitle("Modificar Seguro");
}
/**
 * @brief Destructor
 */
moficaseguro::~moficaseguro()
{
    delete ui;
}

/**
 * @brief Guarda los cambios realizdos en el seguro del paciente y abre la ventana v_exito
 */
void moficaseguro::on_pushButton_guardar_clicked()
{
    Paciente p;
    std::fstream archivo;

    archivo.open("./pacientes.txt", std::ios::in | std::ios::out);

    //modificar
    archivo.seekg(((this->n_historia-1)*80), archivo.beg);

    archivo >> p;

    p.ingresar_seguro(ui->lineEdit_seguro->text().toStdString());

    archivo.seekg(0, archivo.beg);
    archivo.seekp(((this->n_historia-1)*80), archivo.beg);

    archivo << p;

    QMessageBox::information(this, tr("Éxito"), tr("Seguro modificado"));

    v_buscapaciente *moficaseguro = new v_buscapaciente;
    moficaseguro->show();
    this->close();
}
