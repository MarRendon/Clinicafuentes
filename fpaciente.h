#ifndef FPACIENTE_H
#define FPACIENTE_H

#include <QWidget>
#include <string>

namespace Ui {
class fpaciente;
}
/**
 * @brief Muestra una tabla con todos los pacientes con la fecha de nacimiento buscada
 * en caso de saber el número de historia
 */
class fpaciente : public QWidget
{
    Q_OBJECT

public:

    void resizeEvent (QResizeEvent* event)
    {
        m_pPalette->setBrush(QPalette::Background,QBrush(m_pPixmap->scaled(width(),height())));
        setPalette(*m_pPalette);
    };

    /**
     * @brief fechaN fecha utilizada para buscar numero de historia del paciente
     */
    QString fechaN;

    explicit fpaciente(QString*, QWidget *parent = 0);
    ~fpaciente();

private slots:

    void on_volver_clicked();

    void on_buscar_clicked();

private:
    Ui::fpaciente *ui;

    QPixmap* m_pPixmap;
    QPalette* m_pPalette;
};

#endif // FPACIENTE_H
