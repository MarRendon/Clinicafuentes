#-------------------------------------------------
#
# Project created by QtCreator 2016-04-16T18:52:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ClinicaFuentes
TEMPLATE = app


SOURCES += main.cpp\
        v_principal.cpp \
    v_buscapaciente.cpp \
    v_paciente.cpp \
    v_programa.cpp \
    muestrapaciente.cpp \
    moficaseguro.cpp \
    modificatlf.cpp \
    datoscita.cpp \
    cita.cpp \
    paciente.cpp \
    fpaciente.cpp \
    muestracita.cpp

HEADERS  += v_principal.h \
    v_buscapaciente.h \
    v_paciente.h \
    v_programa.h \
    muestrapaciente.h \
    moficaseguro.h \
    modificatlf.h \
    datoscita.h \
    cita.h \
    paciente.h \
    fpaciente.h \
    muestracita.h

FORMS    += v_principal.ui \
    v_buscapaciente.ui \
    v_paciente.ui \
    v_programa.ui \
    muestrapaciente.ui \
    moficaseguro.ui \
    modificatlf.ui \
    datoscita.ui \
    fpaciente.ui \
    muestracita.ui

