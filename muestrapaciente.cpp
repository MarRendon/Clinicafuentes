#include "muestrapaciente.h"
#include "ui_muestrapaciente.h"
#include "paciente.h"
#include "v_buscapaciente.h"
#include "moficaseguro.h"
#include "modificatlf.h"
#include "v_programa.h"
#include "paciente.h"
#include <string>
#include <QMessageBox>

/**
 * @brief asigna el numero de historia usando el operador de desreferencia, asigna los datos del paciente que seran mostrados y asigna un fondo a la ventana MuestraPaciente
 * muestra los datos del paciente buscado
 * @param nhistoria
 * @param a
 * @param parent
 */
MuestraPaciente::MuestraPaciente(int *nhistoria, Paciente *a, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MuestraPaciente)
{
    //Muestra los datos del paciente
    ui->setupUi(this);

    m_pPalette	= new QPalette();
    m_pPixmap		= new QPixmap("/home/maralba/Imágenes/bg3.jpg");

    m_pPalette->setBrush(QPalette::Background,QBrush(*m_pPixmap));
    setPalette(*m_pPalette);

    this->p=*a;
    this->n_historia=*nhistoria;

    ui->label_muestranombre->setText(QString::fromStdString(p.ver_nombre()));
    ui->muestrafechan->setText(QString::fromStdString(p.ver_fecha_nacimiento()));
    ui->label_muestranhistoria->setText(QString::number((this->n_historia)));
    ui->label_muestraseguro->setText(QString::fromStdString(p.ver_seguro()));
    ui->label_muestratlf->setText(QString::fromStdString(p.ver_telefono()));
    this->setWindowTitle("Paciente Regular");
}

MuestraPaciente::~MuestraPaciente()
{
    delete ui;
}

/**
 * @brief Regresa a la ventana v_buscapaciente
 */
void MuestraPaciente::on_pushButton_volver_clicked()
{
    v_buscapaciente *MuestraPaciente = new v_buscapaciente;
    MuestraPaciente->show();
    this->close();
}

/**
 * @brief Abre la ventana para modificar el seguro del paciente buscado
 */

void MuestraPaciente::on_pushButton_modificar_clicked()
{
    //modificar seguro

    moficaseguro *MuestraPaciente = new moficaseguro(&this->n_historia);
    MuestraPaciente->show();
    this->close();

}

/**
 * @brief Abre la ventana para modificar el telefono del paciente buscado
 */
void MuestraPaciente::on_pushButton_modificar_2_clicked()
{
    //modificar telefono

    modificatlf *MuestraPaciente = new modificatlf(&this->n_historia);
    MuestraPaciente->show();
    this->close();
}

/**
 * @brief Abre la ventana v_programa para hacer una cita
 */
void MuestraPaciente::on_pushButton_hacercita_clicked()
{
   // int c=0;
    v_programa *MuestraPaciente =new v_programa(&this->n_historia);//, &c);
    MuestraPaciente->show();
    //this->close();
}

/**
 * @brief Elimina datos del paciente buscado del archivo "citas.txt" y llama a la ventana pacElimin
 */

void MuestraPaciente::on_pushButton_eliminar_clicked()
{
    Paciente p;

    std::fstream archivo;
    archivo.open("./pacientes.txt", std::ios::in | std::ios::out);

    archivo.seekp(((this->n_historia-1)*80), archivo.beg);

    archivo << p;

    QMessageBox::information(this, tr("Éxito"), tr("Historia eliminada con éxito"));

    v_buscapaciente *MuestraPaciente= new v_buscapaciente;
    MuestraPaciente->show();
    this->close();
}

