#ifndef PACIENTE_H
#define PACIENTE_H
#include<string>

using std::string;
/**
 * @brief declaraciones de los metodos de la clase Paciente
 */
class Paciente
{

private:

    string nombre; /*nombre del paciente*/
    string seguro;
    string telefono;
    int numero_historia;
    string fecha_nacimiento;

public:

    /**
     * @brief Constructor por defecto
     */
    Paciente();

    /**
     * @brief Constructor de copia
     */
    Paciente(const Paciente&);

    ~Paciente(){}

    /**
     * @brief guarda el nombre del paciente
     */
    void ingresar_nombre(string);

    /**
     * @brief ver_nombre
     * @return nombre del paciente
     */
    std::string ver_nombre() const;

    /**
     * @brief guarda la fecha de nacimiento del paciente
     */
    void ingresar_fecha_nacimiento(string);

    /**
     * @brief ver_fecha_nacimiento
     * @return returna fecha de nacimiento del paciente
     */
    string ver_fecha_nacimiento()  const;

    /**
     * @brief guarda seguro del paciente
     */
    void ingresar_seguro(string);

    /**
     * @brief ver_seguro
     * @return seguro del paciente
     */
    string ver_seguro() const;

    /**
     * @brief guarda telefono del paciente
     */
    void ingresar_telefono(string);

    /**
     * @brief ver_telefono
     * @return numero de telefono del paciente
     */
    string ver_telefono() const;

    /**
     * @brief guarda número de historia
     */
    void ingresar_numero_historia(int);

    /**
     * @brief ver_numero_historia
     * @return numero de historia asignado al paciente
     */
    int ver_numero_historia() const;
};
/**
 * @brief operator << sebrecarga del operador << para insertar los datos de la clase en un archivo
 */
std::ostream& operator <<(std::ostream&, const Paciente&);

/**
 * @brief operator >> sebrecarga del operador >> para extraer los datos de la clase de un archivo
 */
std::istream& operator >>(std::istream&, Paciente&);

#endif // PACIENTE_H
