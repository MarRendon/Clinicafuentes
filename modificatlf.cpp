#include "modificatlf.h"
#include "ui_modificatlf.h"
#include "paciente.h"
#include <fstream>
#include <QMessageBox>
#include <v_buscapaciente.h>
#include <QRegExpValidator>
/**
 * @brief asigna un fondo a la ventana modificatlf
 * @param nhistoria
 * @param parent
 */
modificatlf::modificatlf(int *nhistoria, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::modificatlf)
{
    this->n_historia=*nhistoria;

    ui->setupUi(this);

    m_pPalette	= new QPalette();
    m_pPixmap		= new QPixmap("/home/maralba/Imágenes/imagen15.jpg");

    m_pPalette->setBrush(QPalette::Background,QBrush(*m_pPixmap));
    setPalette(*m_pPalette);

    QRegExp rx1("^[0-9]*$");
    QRegExpValidator *valiT= new QRegExpValidator(rx1, this);
    ui->lineEdit_telefono->setValidator(valiT);

    setWindowTitle("Modificar Seguro");
}

modificatlf::~modificatlf()
{
    delete ui;
}

/**
 * @brief Guarda el nuevo telefono del paciente en el archivo y envia los datos
 * modificados a la ventana v_exito para luego ser mostrados en muestrapaciente
 */
void modificatlf::on_pushButton_guardar_clicked()
{
    Paciente p;
    std::fstream archivo;

    archivo.open("./pacientes.txt", std::ios::in | std::ios::out);

    //modificar
    archivo.seekg(((this->n_historia-1)*80), archivo.beg);

    archivo >> p;

    p.ingresar_telefono(ui->lineEdit_telefono->text().toStdString());

    archivo.seekg(0, archivo.beg);
    archivo.seekp(((this->n_historia-1)*80), archivo.beg);

    archivo << p;

    QMessageBox::information(this, tr("Éxito"), tr("Telefono modificado"));

    v_buscapaciente *modificatlf = new v_buscapaciente;
    modificatlf->show();

    this->close();
}
