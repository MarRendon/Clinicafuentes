#ifndef MUESTRACITA_H
#define MUESTRACITA_H

#include <QWidget>

namespace Ui {
class muestracita;
}

class muestracita : public QWidget
{
    Q_OBJECT

public:

    int n_historia;

    void resizeEvent (QResizeEvent* event)
    {
        m_pPalette->setBrush(QPalette::Background,QBrush(m_pPixmap->scaled(width(),height())));
        setPalette(*m_pPalette);
    };

    explicit muestracita(int *posicion, int *nh, QWidget *parent = 0);
    ~muestracita();

private slots:
    void on_pushButton_clicked();

private:

    QPixmap* m_pPixmap;
    QPalette* m_pPalette;

    Ui::muestracita *ui;
};

#endif // MUESTRACITA_H
