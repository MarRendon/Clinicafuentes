var searchData=
[
  ['v_5fbuscapaciente',['v_buscapaciente',['../classv__buscapaciente.html',1,'v_buscapaciente'],['../classv__buscapaciente.html#a65522bc32417cee1cfc8bebf44f525da',1,'v_buscapaciente::v_buscapaciente()']]],
  ['v_5fpaciente',['v_paciente',['../classv__paciente.html',1,'v_paciente'],['../classv__paciente.html#a8c6f53a6465f3273f4808361ee031a27',1,'v_paciente::v_paciente()']]],
  ['v_5fprincipal',['v_principal',['../classv__principal.html',1,'v_principal'],['../classv__principal.html#afb2e5585e7c4de68ef8e904354e6d421',1,'v_principal::v_principal()']]],
  ['v_5fprograma',['v_programa',['../classv__programa.html',1,'v_programa'],['../classv__programa.html#aaf260909f58dc85e7c422022595287c9',1,'v_programa::v_programa()']]],
  ['ver_5fdoctor',['ver_doctor',['../class_cita.html#a40fdd99f43300690e7b5ce090a2ba2c9',1,'Cita']]],
  ['ver_5ffecha',['ver_fecha',['../class_cita.html#a05700cdb1dde348d31b296ac58bc3bd3',1,'Cita']]],
  ['ver_5ffecha_5fnacimiento',['ver_fecha_nacimiento',['../class_paciente.html#a8aa1a3e587dda58fb31447fae2a4104e',1,'Paciente']]],
  ['ver_5fhora',['ver_hora',['../class_cita.html#a1fb41ef3311a04d7fd2a216387442c5e',1,'Cita']]],
  ['ver_5fmotivo',['ver_motivo',['../class_cita.html#a0642b5324e95ae58ececc2a50d0f9019',1,'Cita']]],
  ['ver_5fn_5fhistoriap',['ver_n_historiap',['../class_cita.html#a1f748135dc2ed58fe3b281e9cda8b4dd',1,'Cita']]],
  ['ver_5fnombre',['ver_nombre',['../class_paciente.html#a630a9f611d08e6f30d097b35cfa85c29',1,'Paciente']]],
  ['ver_5fnumero_5fhistoria',['ver_numero_historia',['../class_paciente.html#ac626de4adfd9c3686ab6ea407a650b88',1,'Paciente']]],
  ['ver_5fposicion',['ver_posicion',['../class_cita.html#ab8eb710db6a0b1084268470a4245703d',1,'Cita']]],
  ['ver_5fseguro',['ver_seguro',['../class_paciente.html#abb04e373f64821aa82e77628c321f66e',1,'Paciente']]],
  ['ver_5ftelefono',['ver_telefono',['../class_paciente.html#accf572b90d505a8f786a7f4437d6c26a',1,'Paciente']]]
];
