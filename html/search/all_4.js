var searchData=
[
  ['ingresar_5fdoctor',['ingresar_doctor',['../class_cita.html#a4ae5cbf4733007a549d4b79816771dfb',1,'Cita']]],
  ['ingresar_5ffecha',['ingresar_fecha',['../class_cita.html#a2376785e166b36ea8ee953470a15f6d8',1,'Cita']]],
  ['ingresar_5ffecha_5fnacimiento',['ingresar_fecha_nacimiento',['../class_paciente.html#a2e85a1701add88767dfd8114b9279a12',1,'Paciente']]],
  ['ingresar_5fhora',['ingresar_hora',['../class_cita.html#a265cf64fa7845b82da59164af2022456',1,'Cita']]],
  ['ingresar_5fmotivo',['ingresar_motivo',['../class_cita.html#a97bf9b8f1a5b03f94f73b5e651286837',1,'Cita']]],
  ['ingresar_5fn_5fhistoriap',['ingresar_n_historiap',['../class_cita.html#ad02b05caa06b1858003bf766d3ef8a1a',1,'Cita']]],
  ['ingresar_5fnombre',['ingresar_nombre',['../class_paciente.html#a40ac35522facb725ed9d66d1b90d985c',1,'Paciente']]],
  ['ingresar_5fnumero_5fhistoria',['ingresar_numero_historia',['../class_paciente.html#addc3a5adf33bdd550cd80aaeda491005',1,'Paciente']]],
  ['ingresar_5fposicion',['ingresar_posicion',['../class_cita.html#a87d138aa268582d29d75eb370116531f',1,'Cita']]],
  ['ingresar_5fseguro',['ingresar_seguro',['../class_paciente.html#a92e071731a090f96b34f5b23da9a3eda',1,'Paciente']]],
  ['ingresar_5ftelefono',['ingresar_telefono',['../class_paciente.html#a54da0aafc0f8ba8645fb6e66f6d7dc87',1,'Paciente']]]
];
