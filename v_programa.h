#ifndef V_PROGRAMA_H
#define V_PROGRAMA_H

#include <QWidget>
#include <QMainWindow>
#include<fstream>
#include<iostream>
#include <QDebug>
#include "paciente.h"

namespace Ui {
class v_programa;
}
/**
 * @brief The v_programa class, Crea la ventana que muestra el calendario de citas disponible y ocupadas
 */
class v_programa : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief n_historia, guarda el número de historia del paciente que esta siendo atendido en ese momento
     */

    void resizeEvent (QResizeEvent* event)
    {
        m_pPalette->setBrush(QPalette::Background,QBrush(m_pPixmap->scaled(width(),height())));
        setPalette(*m_pPalette);
    };

    /**
     * @brief n_historia almacena el numero de historia del paciente que desea hacer la cita
     */
    int n_historia;
    /**
     * @brief ho almacena las horas ocupadas para evitas 2 citas a la misma hora
     */
    int ho[8];

    explicit v_programa(int *nh, QWidget *parent = 0);
    ~v_programa();

private:
    Ui::v_programa *ui;

    QPixmap* m_pPixmap;
    QPalette* m_pPalette;

private slots:
    void on_calendar_3_selectionChanged();
    void on_guardar_3_clicked();
    void on_volver_clicked();
    void on_eliminar_clicked();
    void on_buscar_clicked();
};

#endif // V_PROGRAMA_H
