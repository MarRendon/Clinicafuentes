#include "v_paciente.h"
#include "ui_v_paciente.h"
#include "v_principal.h"
#include "paciente.h"
#include "string.h"
#include <fstream>
#include <QRegExpValidator>
#include <QMessageBox>

using namespace std;
/**
 * @brief Constructor, asigna un fondo a la venta v_paciente
 * @param parent
 */
v_paciente::v_paciente(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::v_paciente)
{
    ui->setupUi(this);
    QRegExp rx("^([a-z]+[,.]?[ ]?|[a-z]+['-]?)+$");
    QRegExpValidator *valiNombre= new QRegExpValidator(rx, this);
    ui->lineEdit_nombre->setValidator(valiNombre);
    ui->lineEdit_seguro->setValidator(valiNombre);

    QRegExp rx1("^[0-9]*$");
    QRegExpValidator *valiT= new QRegExpValidator(rx1, this);
    ui->lineEdit_telefono->setValidator(valiT);

    ui->fechan->setDisplayFormat("dd/MM/yyyy");

    m_pPalette	= new QPalette();
    m_pPixmap		= new QPixmap("/home/maralba/Imágenes/bg3.jpg");

    m_pPalette->setBrush(QPalette::Background,QBrush(*m_pPixmap));
    setPalette(*m_pPalette);
    setWindowTitle("Paciente Nuevo");
}

v_paciente::~v_paciente()
{
    delete ui;
}

/**
 * @brief Regresa a la ventana principal
 */
void v_paciente::on_pushButton_clicked()
{
    v_principal *v_paciente = new v_principal;
    v_paciente->show();
    this->close();
}

/**
 * @brief Guarda los datos del nuevo paciente
 */
void v_paciente::on_pushButton_npaciente_guardar_clicked()
{

    //guarda los datos del nuevo paciente en el archivo pacientes.txt

    std::string nombre, actual;
    int tam, error=0;

    /** valida que se ha escrito el nombre y guarda el nombre del paciente */

    if( ui->lineEdit_nombre->text().isEmpty())
    {
      QMessageBox::information(this, tr("ERROR"), tr("Debe indicar Nombre del paciente en minúsculas"));
      error++;
    }
    else
    {
      p.ingresar_nombre(ui->lineEdit_nombre->text().toStdString());
    }

    /** Guarda la fecha de nacimiento*/

    QString str = ui->fechan->date().toString("dd/MM/yyyy");
    actual=str.toStdString();
    p.ingresar_fecha_nacimiento(actual);

    /** Valida que se ha escrito en la casilla de seguro y guarda el seguro del paciente*/
    nombre= ui->lineEdit_seguro->text().toStdString();
    if(ui->lineEdit_seguro->text().isEmpty())
    {
      QMessageBox::information(this, tr("ERROR"), tr("Debe indicar el seguro del paciente"));
      error++;
    }
    else
    {
         p.ingresar_seguro(nombre);
    }

    /** Valida que se ha escrito en la casilla telefono y lo guarda*/
    nombre= ui->lineEdit_telefono->text().toStdString();
    if(ui->lineEdit_telefono->text().isEmpty())
    {
      QMessageBox::information(this, tr("ERROR"), tr("Debe indicar el número de telefono del paciente"));
      error++;
    }
    else
    {
         p.ingresar_telefono(nombre);
    }

    if(!error)
    {
        std::fstream archivo;
        archivo.open("pacientes.txt",  fstream::out | fstream::app);

        archivo.clear();
        archivo.seekg(0, archivo.end);
        tam = archivo.tellg();

        tam=tam/76;

        p.ingresar_numero_historia(tam+1);

        archivo << p;

        tam=tam+1;

        QMessageBox noc;
        QString er = QString::number(tam);
        QString s = tr("Paciente guardado con éxito\n\n Número de Historia asignada: %1").arg(er);
        noc.setText(s);
        noc.exec();

        v_principal *v_paciente = new v_principal;
        v_paciente->show();
        this->close();
    }
}
