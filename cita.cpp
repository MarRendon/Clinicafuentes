#include<string>
#include<iomanip>
#include<stdlib.h>
#include"cita.h"
#include "fstream"

using std::string;
using std::setw;
using std::left;
using std::right;

/**
 * @brief constructor por defecto
 */
Cita::Cita(){
    this->fecha="NULL";
    this->hora=0;
    this->motivo="NULL";
    this->cod_doctor=0;
    this->n_historiap=0;
    this->posicion=0;

    return;
}

/**
 * @brief constructor de copia
 * @param c
 */
Cita::Cita(const Cita &c){

    this->fecha=c.fecha;
    this->hora=c.hora;
    this->motivo=c.motivo;
    this->cod_doctor=c.cod_doctor;
    this->n_historiap=c.n_historiap;
    this->posicion=c.posicion;

    return;
}

/**
 * @brief Guarda fecha
 * @param f
 * @return fecha
 */
void Cita::ingresar_fecha(string f){

    this->fecha=f;
}

string Cita::ver_fecha() const{	return(this->fecha);}

/**
 * @brief guarda hora
 * @return hora
 */
void Cita::ingresar_hora(int h){

    this->hora=h;
}


int Cita::ver_hora()  const{	return(this->hora);}

/**
 * @brief Guarda motivo
 * @param m
 * @return motivo
 */
void Cita::ingresar_motivo(string m){

    this->motivo=m;
}


string Cita::ver_motivo() const{	return(this->motivo);}

/**
 * @brief guarda el codigo del doctor que atendera la cita
 * @param codd
 * @return cod_doctorl, codigo del doctor
 */
void Cita::ingresar_doctor(int codd){

    this->cod_doctor=codd;
}

int Cita::ver_doctor()  const{	return(this->cod_doctor);}

/**
 * @brief guarda número de historia
 * @param nh
 * @return numero de historia del paciente que asistira a la cita
 */
void Cita::ingresar_n_historiap(int nh){

    this->n_historiap=nh;
}

int Cita::ver_n_historiap()  const{  return(this->n_historiap);}

/**
 * @brief Guarda estado de la cita
 * @param e
 * @return estado de la cita
 */
void Cita::ingresar_posicion(int e){

    this->posicion=e;
}

int Cita::ver_posicion() const{ return(this->posicion);}

/**
 * @brief sobrecarga del operador  <<
 * @param o archivo
 * @param c objeto de la clase Cita
 * @return
 */
std::ostream& operator <<(std::ostream& o, const Cita& c){

    o << left << setw(5) << c.ver_posicion() << ":";
    o << left << setw(10) << c.ver_fecha() << ":";
    o << left << setw(6) << c.ver_hora() << ":";
    o << left << setw(15) << c.ver_motivo() << ":";
    o << left << setw(10) << c.ver_doctor() << ":";
    o << right << setw(5) << c.ver_n_historiap() << "\n";

    return(o);
}

/**
 * @brief sebrecarga del operador >>
 * @param s archivo
 * @param p objeto de la clase Cita
 * @return
 */

std::istream& operator >>(std::istream& s, Cita& p){

    std::string cadenaAuxiliar;
    int f[3];

    std::getline(s, cadenaAuxiliar, ':');

    if (!s.fail()) {
        f[0] = (int)atoi(cadenaAuxiliar.c_str());
    }

    p.ingresar_posicion(f[0]);

    std::getline(s, cadenaAuxiliar, ':');

    if (!s.fail()) {
        p.ingresar_fecha(cadenaAuxiliar);
    }

     std::getline(s, cadenaAuxiliar, ':');

    if (!s.fail()) {
       f[0] = (int)atoi(cadenaAuxiliar.c_str());
    }

    p.ingresar_hora(f[0]);

    std::getline(s, cadenaAuxiliar, ':');

    if (!s.fail()) {
       p.ingresar_motivo(cadenaAuxiliar);
    }

    std::getline(s, cadenaAuxiliar, ':');

    if (!s.fail()) {
       f[0] = (int)atoi(cadenaAuxiliar.c_str());
       p.ingresar_doctor(f[0]);
    }

    std::getline(s, cadenaAuxiliar);

    if (!s.fail()) {
       f[0] = (int)atoi(cadenaAuxiliar.c_str());
       p.ingresar_n_historiap(f[0]);
    }

    return(s);
}
